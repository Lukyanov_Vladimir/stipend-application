package ru.desktop.application.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Класс сущности "Учебный предмет"
 *
 * @author Лукьянов В. А.
 */
@Entity
@Table(name = "subjects")
public class Subject implements Serializable  {

    private static final long serialVersionUID = 3;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "subject_name", nullable = false)
    private String subjectName;

    @Column(name = "teacher_full_name", nullable = false)
    private String fullNameTeacher;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subject", fetch = FetchType.LAZY)
    private List<Performance> performances;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getFullNameTeacher() {
        return fullNameTeacher;
    }

    public void setFullNameTeacher(String fullNameTeacher) {
        this.fullNameTeacher = fullNameTeacher;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    public void addPerformance(Performance performance) {
        performance.setSubject(this);
        performances.add(performance);
    }
}
