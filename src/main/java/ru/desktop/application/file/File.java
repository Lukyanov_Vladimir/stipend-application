package ru.desktop.application.file;

import java.io.*;

/**
 * Класс для работы с файлами
 *
 * @author Лукьянов В. А.
 */
public class File {

    private final String path;

    public File(String path) {
        this.path = path;
    }

    /**
     * Записывает строку в файл
     *
     * @param str - строка
     */
    public void fileWrite(String str) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {

            bufferedWriter.write(str);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Читает строку из файла
     *
     * @return - строка
     */
    public String fileRead() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {

            StringBuilder str = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                str.append(line);
            }

            return str.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
