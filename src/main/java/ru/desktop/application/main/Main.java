package ru.desktop.application.main;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.desktop.application.windows.MainWindow;

/**
 * Главный класс приложения
 *
 * @author Лукьянов В. А.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Запускает стартовое окно приложения
     *
     * @param primaryStage - главное окно
     */
    public void start(Stage primaryStage) {
        MainWindow mainWindow = new MainWindow(primaryStage);
        mainWindow.show();
    }
}
