package ru.desktop.application.interfaces;

import java.util.List;

public interface Service<T> {

    T findById(int id);

    List<T> findAll();

    void save(T essence);

    void update(T essence);

    void delete(T essence);
}
