package ru.desktop.application;

/**
 * Класс настроек для рассчёта стипендии
 *
 * @author Лукьянов В. А.
 */
public class StipendCfg {

    private double minStipend;

    public double getMinStipend() {
        return minStipend;
    }

    public void setMinStipend(double minStipend) {
        this.minStipend = minStipend;
    }
}
