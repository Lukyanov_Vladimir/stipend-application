package ru.desktop.application.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import ru.desktop.application.windows.ErrorWindow;

/**
 * Класс для работы с hibernate
 *
 * @author Лукьянов В. А.
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    /**
     * Метод создаёт фабрику сессий
     */
    public static void buildSessionFactory() {

        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure("cfg/hibernate.cfg.xml").build();

        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            ErrorWindow errorWindow = new ErrorWindow("Невозможно подключиться к базе данных!\nПроверьте интернет соединение.");
            errorWindow.show();
            throw new ExceptionInInitializerError("Initial SessionFactory failed" + e);
        }

    }

    /**
     * Метод возвращает фабрику сессий
     *
     * @return - фабрика сессий
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Метод закрывает подключение к базе данных и фабрику сессий
     */
    public static void shutdown() {
        getSessionFactory().close();
    }

    /**
     * Метод открывает новую сессию
     *
     * @return - новая сессия
     */
    public static Session getSession() {
        return sessionFactory.openSession();
    }
}
