package ru.desktop.application.stipend;

import ru.desktop.application.StipendCfg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Класс для рассчёта стипендии
 *
 * @author Лукьянов В. А.
 */
public class Stipend implements Serializable {

    private final StipendCfg stipendCfg;
    private final HashMap<Integer, Integer> countedMarks;
    private int numMarks;

    public Stipend(StipendCfg stipendCfg) {
        this.stipendCfg = stipendCfg;

        countedMarks = new HashMap<>();
    }

    /**
     * Рассчитывает стипендию
     *
     * @param marks - список с оценками
     * @return - назначенная стипендия
     */
    public double calc(ArrayList<Integer> marks) {
        numMarks = marks.size();

        countMarks(marks);

        if (numMarks == 0)
            return 0;

        double result = calcStipend(0);

        if (!checkTripleOrDeuce()) {
            if (checkAllFives()) {
                result = calcStipend(75);

            } else if (checkFivesMoreThanFours()) {
                result = calcStipend(50);

            } else if (checkFivesEqualFours()) {
                result = calcStipend(25);

            }

        } else {
            result = 0;
        }

        return result;
    }

    /**
     * Сортирует и подсчитывая каждую оценку
     *
     * @param marks - список с оценками
     */
    private void countMarks(ArrayList<Integer> marks) {
        int tempGrade = 2;
        int countGrade = 0;
        for (int i = 0; i < 4; i++) {
            for (int grade : marks) {
                if (grade == tempGrade)
                    countGrade++;
            }
            countedMarks.put(tempGrade, countGrade);
            countGrade = 0;
            tempGrade++;
        }
    }

    /**
     * Проверяет есть ли 2 и 3
     *
     * @return - true или false
     */
    public boolean checkTripleOrDeuce() {
        boolean check = false;

        if (countedMarks.get(2) != 0)
            check = true;

        if (countedMarks.get(3) != 0)
            check = true;

        return check;
    }

    /**
     * Сравнивает количество 5 и 4
     *
     * @return - true или false
     */
    public boolean checkFivesEqualFours() {
        return countedMarks.get(5).equals(countedMarks.get(4));
    }

    /**
     * Проверяет 4 больше чем 5
     *
     * @return - true или false
     */
    public boolean checkFivesMoreThanFours() {
        return countedMarks.get(5) > countedMarks.get(4);
    }

    /**
     * Проверяет количество 5 равняется количеству оценок
     *
     * @return - true или false
     */
    public boolean checkAllFives() {
        return countedMarks.get(5) == numMarks;
    }

    /**
     * Рассчитывает стипендию
     *
     * @param bonusPercent - процент надбавки
     * @return - true или false
     */
    public double calcStipend(int bonusPercent) {
        double stipend = stipendCfg.getMinStipend();

        if (bonusPercent != 0) {
            stipend += stipendCfg.getMinStipend() * bonusPercent / 100;
        }

        return stipend;
    }
}
