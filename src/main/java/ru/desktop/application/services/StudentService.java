package ru.desktop.application.services;

import org.hibernate.Session;
import ru.desktop.application.dao.StudentDao;
import ru.desktop.application.entities.Student;
import ru.desktop.application.interfaces.Service;

import java.util.List;

/**
 * Класс-слой бизнес логики для работы с данными студентов
 *
 * @author Лукьянов В. А.
 */
public class StudentService implements Service<Student> {

    private final StudentDao studentDao;

    public StudentService(Session session) {

        studentDao = new StudentDao(session);
    }

    /**
     * Находит студента по id
     *
     * @param id - id студента
     * @return - объект студента
     */
    @Override
    public Student findById(int id) {
        return studentDao.findById(id);
    }

    /**
     * Находит всех студентов
     *
     * @return - список с успевамостями
     */
    @Override
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    /**
     * Сохраняет студента в бд
     *
     * @param student - студент
     */
    @Override
    public void save(Student student) {
        studentDao.save(student);
    }

    /**
     * Обновляет студента в бд
     *
     * @param student - студент
     */
    @Override
    public void update(Student student) {
        studentDao.update(student);
    }

    /**
     * Удаляет студента в бд
     *
     * @param student - студент
     */
    @Override
    public void delete(Student student) {
        studentDao.delete(student);
    }
}
