package ru.desktop.application.services;

import org.hibernate.Session;
import ru.desktop.application.dao.SubjectDao;
import ru.desktop.application.entities.Subject;
import ru.desktop.application.interfaces.Service;

import java.util.List;

/**
 * Класс-слой бизнес логики для работы с данными учебных предметов
 *
 * @author Лукьянов В. А.
 */
public class SubjectService implements Service<Subject> {

    private final SubjectDao subjectDao;

    public SubjectService(Session session) {

        subjectDao = new SubjectDao(session);
    }

    /**
     * Находит учебный предмет по id
     *
     * @param id - id учебного предмета
     * @return - объект учебного предмета
     */
    @Override
    public Subject findById(int id) {
        return subjectDao.findById(id);
    }

    /**
     * Находит все учебные предметы
     *
     * @return - список учебных предметов
     */
    @Override
    public List<Subject> findAll() {
        return subjectDao.findAll();
    }

    /**
     * Сохраняет учебные предметы в бд
     *
     * @param subject - успеваемость
     */
    @Override
    public void save(Subject subject) {
        subjectDao.save(subject);
    }

    /**
     * Обновляет учебные предметы в бд
     *
     * @param subject - успеваемость
     */
    @Override
    public void update(Subject subject) {
        subjectDao.update(subject);
    }

    /**
     * Удаляет учебные предметы в бд
     *
     * @param subject - успеваемость
     */
    @Override
    public void delete(Subject subject) {
        subjectDao.delete(subject);
    }
}
