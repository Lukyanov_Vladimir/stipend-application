package ru.desktop.application.services;

import org.hibernate.Session;
import ru.desktop.application.dao.PerformanceDao;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.interfaces.Service;

import java.util.List;

/**
 * Класс-слой бизнес логики для работы с данными успевамостей
 *
 * @author Лукьянов В. А.
 */
public class PerformanceService implements Service<Performance> {

    private final PerformanceDao performanceDao;

    public PerformanceService(Session session) {

        performanceDao = new PerformanceDao(session);
    }

    /**
     * Находит успеваемость по id
     *
     * @param id - id успеваемости
     * @return - объект успеваемости
     */
    @Override
    public Performance findById(int id) {
        return performanceDao.findById(id);
    }

    /**
     * Находит все успеваемости
     *
     * @return - список с успевамостями
     */
    @Override
    public List<Performance> findAll() {
        return performanceDao.findAll();
    }

    /**
     * Сохраняет успевавемость в бд
     *
     * @param performance - успеваемость
     */
    @Override
    public void save(Performance performance) {
        performanceDao.save(performance);
    }

    /**
     * Обновляет успевавемость в бд
     *
     * @param performance - успеваемость
     */
    @Override
    public void update(Performance performance) {
        performanceDao.update(performance);
    }

    /**
     * Удаляет успевавемость в бд
     *
     * @param performance - успеваемость
     */
    @Override
    public void delete(Performance performance) {
        performanceDao.delete(performance);
    }
}
