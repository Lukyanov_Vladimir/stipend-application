package ru.desktop.application.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.interfaces.Dao;

import java.util.List;

/**
 * Класс для работы с данными успевамостей
 *
 * @author Лукьянов В. А.
 */
public class PerformanceDao implements Dao<Performance> {

    private final Session session;

    public PerformanceDao(Session session) {
        this.session = session;
    }

    /**
     * Находит успеваемость по id
     *
     * @param id - id успеваемости
     * @return - объект успеваемости
     */
    @Override
    public Performance findById(int id) {
        return session.get(Performance.class, id);
    }

    /**
     * Находит все успеваемости
     *
     * @return - список с успевамостями
     */
    @Override
    public List<Performance> findAll() {
        return (List<Performance>) session.createQuery("FROM Performance").list();
    }

    /**
     * Сохраняет успевавемость в бд
     *
     * @param performance - успеваемость
     */
    @Override
    public void save(Performance performance) {
        Transaction tr = session.beginTransaction();
        session.save(performance);
        tr.commit();
    }

    /**
     * Обновляет успевавемость в бд
     *
     * @param performance - успеваемость
     */
    @Override
    public void update(Performance performance) {
        Transaction tr = session.beginTransaction();
        session.update(performance);
        tr.commit();
    }

    /**
     * Удаляет успевавемость в бд
     *
     * @param performance - успеваемость
     */
    @Override
    public void delete(Performance performance) {
        Transaction tr = session.beginTransaction();
        session.delete(performance);
        tr.commit();
    }
}
