package ru.desktop.application.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.desktop.application.entities.Subject;
import ru.desktop.application.interfaces.Dao;

import java.util.List;

/**
 * Класс для работы с данными учебных предметов
 *
 * @author Лукьянов В. А.
 */
public class SubjectDao implements Dao<Subject> {

    private final Session session;

    public SubjectDao(Session session) {
        this.session = session;
    }

    /**
     * Находит учебный предмет по id
     *
     * @param id - id учебного предмета
     * @return - объект учебного предмета
     */
    @Override
    public Subject findById(int id) {
        return session.get(Subject.class, id);
    }

    /**
     * Находит все учебные предметы
     *
     * @return - список учебных предметов
     */
    @Override
    public List<Subject> findAll() {
        return (List<Subject>) session.createQuery("FROM Subject").list();
    }

    /**
     * Сохраняет учебные предметы в бд
     *
     * @param subject - успеваемость
     */
    @Override
    public void save(Subject subject) {
        Transaction tr = session.beginTransaction();
        session.save(subject);
        tr.commit();
    }

    /**
     * Обновляет учебные предметы в бд
     *
     * @param subject - успеваемость
     */
    @Override
    public void update(Subject subject) {
        Transaction tr = session.beginTransaction();
        session.update(subject);
        tr.commit();
    }

    /**
     * Удаляет учебные предметы в бд
     *
     * @param subject - успеваемость
     */
    @Override
    public void delete(Subject subject) {
        Transaction tr = session.beginTransaction();
        session.delete(subject);
        tr.commit();
    }
}
