package ru.desktop.application.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.desktop.application.entities.Student;
import ru.desktop.application.interfaces.Dao;

import java.util.List;

/**
 * Класс для работы с данными студентов
 *
 * @author Лукьянов В. А.
 */
public class StudentDao implements Dao<Student> {

    private final Session session;

    public StudentDao(Session session) {
        this.session = session;
    }

    /**
     * Находит студента по id
     *
     * @param id - id студента
     * @return - объект студента
     */
    @Override
    public Student findById(int id) {
        return session.get(Student.class, id);
    }

    /**
     * Находит всех студентов
     *
     * @return - список с успевамостями
     */
    @Override
    public List<Student> findAll() {
        return (List<Student>) session.createQuery("FROM Student").list();
    }

    /**
     * Сохраняет студента в бд
     *
     * @param student - студент
     */
    @Override
    public void save(Student student) {
        Transaction tr = session.beginTransaction();
        session.save(student);
        tr.commit();
    }

    /**
     * Обновляет студента в бд
     *
     * @param student - студент
     */
    @Override
    public void update(Student student) {
        Transaction tr = session.beginTransaction();
        session.update(student);
        tr.commit();
    }

    /**
     * Удаляет студента в бд
     *
     * @param student - студент
     */
    @Override
    public void delete(Student student) {
        Transaction tr = session.beginTransaction();
        session.delete(student);
        tr.commit();
    }
}
