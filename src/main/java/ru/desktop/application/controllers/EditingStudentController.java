package ru.desktop.application.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.desktop.application.entities.Student;
import ru.desktop.application.services.StudentService;
import ru.desktop.application.tables.StudentsTable;
import ru.desktop.application.utils.HibernateUtil;

/**
 * Класс-контроллер окна редактирвания данных студента
 *
 * @author Лукьянов В. А.
 */
public class EditingStudentController {

    @FXML
    private TextField fullName;

    @FXML
    private TextField address;

    @FXML
    private TextField phoneNumber;

    @FXML
    private Button saveBtn;

    private TableView<Student> tableViewStudents;

    private int studentNumber;

    public void setTableViewStudents(TableView<Student> tableViewStudents) {
        this.tableViewStudents = tableViewStudents;
    }

    public void setStudentNumber(int studentNumber) {
        this.studentNumber = studentNumber;
    }

    @FXML
    public void initialize() {
        Session session = HibernateUtil.getSession();
        StudentService studentService = new StudentService(session);
        fillFields(studentService.findById(studentNumber));
        session.close();

        saveBtn.setOnAction(event -> {
            Session session2 = HibernateUtil.getSession();

            StudentService studentService2 = new StudentService(session2);
            Student student = studentService2.findById(studentNumber);
            studentService2.update(editStudent(student));

            session2.close();

            showTableStudents();
        });
    }

    /**
     * Редактирует объект студента
     *
     * @return - объект студента
     */
    private Student editStudent(Student student) {
        student.setFullName(fullName.getText());
        student.setAddress(address.getText());
        student.setPhoneNumber(parseLong(phoneNumber.getText()));

        return student;
    }

    /**
     * Преобразует строку к типу long
     *
     * @param str - строка
     * @return - long
     */
    private long parseLong(String str) {
        return Long.parseLong(str);
    }

    /**
     * Отображает таблицу с данными студентов
     */
    private void showTableStudents() {
        StudentsTable studentsTable = new StudentsTable(tableViewStudents);
        studentsTable.show();
    }

    /**
     * Заполняет поля ввода данными
     *
     * @param student - студент
     */
    private void fillFields(Student student) {
        fillField(fullName, student.getFullName());
        fillField(address, student.getAddress());
        fillField(phoneNumber, String.valueOf(student.getPhoneNumber()));
    }

    /**
     * Заполняет поле ввода данными
     *
     * @param textField - поле ввода
     * @param text - текст
     */
    private void fillField(TextField textField, String text) {
        textField.setText(text);
    }
}
