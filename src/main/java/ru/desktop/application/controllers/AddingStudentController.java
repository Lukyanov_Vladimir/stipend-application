package ru.desktop.application.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.desktop.application.entities.Student;
import ru.desktop.application.services.StudentService;
import ru.desktop.application.tables.StudentsTable;
import ru.desktop.application.utils.HibernateUtil;

/**
 * Класс-контроллер окна добавления успеваемости
 *
 * @author Лукьянов В. А.
 */
public class AddingStudentController {

    @FXML
    private TextField fullName;

    @FXML
    private TextField address;

    @FXML
    private TextField phoneNumber;

    @FXML
    private Button addBtn;

    private TableView<Student> tableViewStudents;

    public void setTableViewStudents(TableView<Student> tableViewStudents) {
        this.tableViewStudents = tableViewStudents;
    }

    @FXML
    public void initialize() {
        addBtn.setOnAction(event -> {
            Session session = HibernateUtil.getSession();

            StudentService studentService = new StudentService(session);
            studentService.save(createObjectStudent());
            session.close();

            showTableStudents();
            fieldsClearText();
        });
    }

    /**
     * Создаёт объект студента
     *
     * @return - объект студента
     */
    private Student createObjectStudent() {
        Student student = new Student();
        student.setFullName(fullName.getText());
        student.setPhoneNumber(parseLong(phoneNumber.getText()));
        student.setAddress(address.getText());

        return student;
    }

    /**
     * Преобразует строку к типу long
     *
     * @param str - строка
     * @return - long
     */
    private long parseLong(String str) {
        return Long.parseLong(str);
    }

    /**
     * Очищает поле ввода
     *
     * @param textField - поле ввода данных
     */
    private void clearField(TextField textField) {
        textField.clear();
    }

    /**
     * Очищает поля ввода
     */
    private void fieldsClearText() {
        clearField(fullName);
        clearField(phoneNumber);
        clearField(address);
    }

    /**
     * Отображает таблицу с данными студентов
     */
    private void showTableStudents() {
        StudentsTable studentsTable = new StudentsTable(tableViewStudents);
        studentsTable.show();
    }
}
