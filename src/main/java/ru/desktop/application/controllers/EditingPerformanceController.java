package ru.desktop.application.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.entities.Subject;
import ru.desktop.application.services.PerformanceService;
import ru.desktop.application.services.SubjectService;
import ru.desktop.application.tables.PerformanceTable;
import ru.desktop.application.tables.SubjectsTable;
import ru.desktop.application.utils.HibernateUtil;

/**
 * Класс-контроллер окна редактирвоания успеваемости
 *
 * @author Лукьянов В. А    .
 */
public class EditingPerformanceController {

    @FXML
    private TextField studentNumber;

    @FXML
    private TextField subjectNumber;

    @FXML
    private TextField mark;

    @FXML
    private Button saveBtn;

    @FXML
    private TableView<Subject> tableViewSubjects;

    private TableView<Performance> tableViewPerformances;

    private int performanceNumber;

    public void setTableViewPerformances(TableView<Performance> tableViewPerformances) {
        this.tableViewPerformances = tableViewPerformances;
    }

    public void setPerformanceNumber(int performanceNumber) {
        this.performanceNumber = performanceNumber;
    }

    @FXML
    public void initialize() {
        fillTableViewSubjects();

        Session session = HibernateUtil.getSession();
        PerformanceService performanceService = new PerformanceService(session);
        Performance performance = performanceService.findById(performanceNumber);
        session.close();

        fillFields(performance.getSubject(), performance);

        saveBtn.setOnAction(event -> {
            Session session2 = HibernateUtil.getSession();

            PerformanceService performanceService2 = new PerformanceService(session2);
            performanceService2.update(editPerformance(session2, performance));

            session2.close();

            showTablePerformances();
        });
    }

    /**
     * Отображает таблицу с учебными предметами
     */
    private void fillTableViewSubjects() {
        SubjectsTable subjectsTable = new SubjectsTable(tableViewSubjects);
        subjectsTable.show();
    }

    /**
     * Редактирует объект успеваемость
     *
     * @param session - сессия
     * @return - объект успевамости
     */
    private Performance editPerformance(Session session, Performance performance) {
        performance.setMark(parseInt(mark.getText()));
        SubjectService subjectService = new SubjectService(session);
        Subject subject = subjectService.findById(parseInt(subjectNumber.getText()));
        performance.setSubject(subject);

        return performance;
    }

    /**
     * Преобразует строку к типу int
     *
     * @param str - строка
     * @return - int
     */
    private Integer parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Отображает таблицу с успеваемостями
     */
    private void showTablePerformances() {
        PerformanceTable performanceTable = new PerformanceTable(tableViewPerformances);
        performanceTable.show();
    }

    /**
     * Заполняет поля ввода данными
     *
     * @param subject - учебный предмет
     * @param performance - успеваемость
     */
    private void fillFields(Subject subject, Performance performance) {
        fillField(subjectNumber, String.valueOf(subject.getId()));
        fillField(studentNumber, String.valueOf(performance.getStudent().getId()));
        fillField(mark, String.valueOf(performance.getMark()));
    }

    /**
     * Заполняет поле ввода данными
     *
     * @param textField - поле ввода
     * @param text - текст
     */
    private void fillField(TextField textField, String text) {
        textField.setText(text);
    }
}
