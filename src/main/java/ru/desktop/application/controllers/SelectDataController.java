package ru.desktop.application.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ru.desktop.application.Tables;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.entities.Student;
import ru.desktop.application.windows.EditingPerformanceWindow;
import ru.desktop.application.windows.EditingStudentWindow;
import ru.desktop.application.windows.ErrorWindow;

/**
 * Класс-контроллер окна выбора данных
 *
 * @author Лукьянов В. А.
 */
public class SelectDataController {

    @FXML
    private TextField number;

    @FXML
    private Button selectBtn;

    private TableView<?> tableView;

    private Tables currentTable;

    private Stage stage;

    public void setTableView(TableView<?> tableView) {
        this.tableView = tableView;
    }

    public void setCurrentTable(Tables currentTable) {
        this.currentTable = currentTable;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        selectBtn.setOnAction(event -> {
            String number = this.number.getText();

            if (number == null) {
                ErrorWindow errorWindow = new ErrorWindow("Пожалуйста введите номер записи!");
                errorWindow.show();

            } else {
                switch (currentTable) {
                    case STUDENTS:
                        EditingStudentWindow editingStudentWindow = new EditingStudentWindow((TableView<Student>) tableView, parseInt(number));
                        editingStudentWindow.show();
                        close();
                        break;

                    case PERFORMANCES:
                        EditingPerformanceWindow editingPerformanceWindow = new EditingPerformanceWindow((TableView<Performance>) tableView, parseInt(number));
                        editingPerformanceWindow.show();
                        close();
                        break;
                }
            }

            this.number.clear();
        });
    }

    /**
     * Преобразует строку к типу int
     *
     * @param str - строка
     * @return - int
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Закрывает окно выбора данных
     */
    private void close() {
        stage.close();
    }
}
