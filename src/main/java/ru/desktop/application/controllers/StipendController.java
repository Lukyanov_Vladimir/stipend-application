package ru.desktop.application.controllers;

import com.google.gson.Gson;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import org.hibernate.Session;
import ru.desktop.application.StipendCfg;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.entities.Student;
import ru.desktop.application.file.File;
import ru.desktop.application.services.StudentService;
import ru.desktop.application.stipend.Stipend;
import ru.desktop.application.tables.StipendTable;
import ru.desktop.application.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс-контроллер окна вывода данных о назначенной стипендии
 *
 * @author Лукьянов В. А.
 */
public class StipendController {

    @FXML
    private TableView<Student> tableView;

    @FXML
    public void initialize() {
        Session session = HibernateUtil.getSession();
        StudentService studentService = new StudentService(session);
        List<Student> students = studentService.findAll();

        Gson gson = new Gson();
        File file = new File("src\\main\\resources\\json\\settings.json");

        Stipend stipend = new Stipend(gson.fromJson(file.fileRead(), StipendCfg.class));

        for (Student student : students) {
            ArrayList<Integer> marks = new ArrayList<>();

            for (Performance performance:student.getPerformances()) {
                marks.add(performance.getMark());
            }

            student.setStipend(stipend.calc(marks));
        }

        StipendTable stipendTable = new StipendTable(tableView, students);
        stipendTable.show();
    }
}
