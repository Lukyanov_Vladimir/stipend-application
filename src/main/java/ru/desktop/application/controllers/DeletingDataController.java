package ru.desktop.application.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.desktop.application.Tables;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.entities.Student;
import ru.desktop.application.services.PerformanceService;
import ru.desktop.application.services.StudentService;
import ru.desktop.application.tables.PerformanceTable;
import ru.desktop.application.tables.StudentsTable;
import ru.desktop.application.utils.HibernateUtil;
import ru.desktop.application.windows.ErrorWindow;

/**
 * Класс-контроллер окна удаления данных
 *
 * @author Лукьянов В. А.
 */
public class DeletingDataController {

    @FXML
    private TextField number;

    @FXML
    private Button deleteBtn;

    private TableView<?> tableView;

    private Tables currentTable;

    public void setTableView(TableView<?> tableView) {
        this.tableView = tableView;
    }

    public void setCurrentTable(Tables currentTable) {
        this.currentTable = currentTable;
    }

    @FXML
    public void initialize() {
        deleteBtn.setOnAction(event -> {
            String number = this.number.getText();

            if (number == null) {
                ErrorWindow errorWindow = new ErrorWindow("Введены некорректные данные!");
                errorWindow.show();

            } else {
                Session session = HibernateUtil.getSession();

                switch (currentTable) {
                    case STUDENTS:
                        StudentService studentService = new StudentService(session);
                        studentService.delete(studentService.findById(parseInt(number)));
                        showTableStudents();
                        break;

                    case PERFORMANCES:
                        PerformanceService performanceService = new PerformanceService(session);
                        performanceService.delete(performanceService.findById(parseInt(number)));
                        showTablePerformances();
                        break;
                }
                session.close();
            }

            this.number.clear();
        });
    }

    /**
     * Преобразует строку к типу int
     *
     * @param str - строка
     * @return - int
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Отображает таблицу с данными студентов
     */
    private void showTableStudents() {
        StudentsTable studentsTable = new StudentsTable((TableView<Student>) tableView);
        studentsTable.show();
    }

    /**
     * Отображает таблицу с данными об успеваемости
     */
    private void showTablePerformances() {
        PerformanceTable performanceTable = new PerformanceTable((TableView<Performance>) tableView);
        performanceTable.show();
    }
}
