package ru.desktop.application.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ErrorWindowController {

    @FXML
    private Button okBtn;

    @FXML
    private Label text;

    private Stage stage;

    private String errorText;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    @FXML
    public void initialize() {
        text.setText(errorText);

        okBtn.setOnAction(event -> {
            close();
        });
    }

    /**
     * Закрывает окно ошибки
     */
    private void close() {
        stage.close();
    }
}
