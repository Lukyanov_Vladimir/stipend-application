package ru.desktop.application.controllers;

import javafx.animation.RotateTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import ru.desktop.application.Tables;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.entities.Student;
import ru.desktop.application.tables.PerformanceTable;
import ru.desktop.application.tables.StudentsTable;
import ru.desktop.application.utils.HibernateUtil;
import ru.desktop.application.windows.*;

/**
 * Класс-контроллер главного окна приложения
 *
 * @author Лукьянов В. А.
 */
public class MainController {

    @FXML
    private TableView<Student> tableViewStudents;

    @FXML
    private TableView<Performance> tableViewPerformance;

    @FXML
    private BorderPane mainMenu;

    @FXML
    private Button editModeBtn;

    @FXML
    private Button studentsBtn;

    @FXML
    private Button performanceBtn;

    @FXML
    private Button reportStipendBtn;

    /*@FXML
    private Button saveLogBtn;*/

    @FXML
    private Button settingsBtn;

    @FXML
    private ImageView settingsIcon;

    @FXML
    private Button exitBtn;

    @FXML
    private BorderPane editMode;

    @FXML
    private Button addBtn;

    @FXML
    private Button deleteBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button exitEditorModeBtn;

    private Stage primaryStage;

    private Tables currentTable = null;

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @FXML
    public void initialize() {
        startHibernate();

        setSettingsBtnAnimation();

        exitBtn.setOnAction(event -> shutdownApp());

        editModeBtn.setOnAction(event -> {
            if (currentTable == null) {
                ErrorWindow errorWindow = new ErrorWindow("Таблица не выбрана!\nПожалуйста выберете таблицу");
                errorWindow.show();
            } else {
                switchMode(true, false);
            }
        });

        exitEditorModeBtn.setOnAction(event -> {
            switchMode(false, true);
        });

        studentsBtn.setOnAction(event -> {
            StudentsTable studentsTable = new StudentsTable(tableViewStudents);
            studentsTable.show();
            switchVisibleTables(true, false);
            currentTable = Tables.STUDENTS;
        });

        performanceBtn.setOnAction(event -> {
            PerformanceTable performanceTable = new PerformanceTable(tableViewPerformance);
            performanceTable.show();
            switchVisibleTables(false, true);
            currentTable = Tables.PERFORMANCES;
        });

        addBtn.setOnAction(event -> {
            addData();
        });

        editBtn.setOnAction(event -> {
            editData();
        });

        deleteBtn.setOnAction(event -> {
            deleteData();
        });

        reportStipendBtn.setOnAction(event -> {
            StipendWindow stipendWindow = new StipendWindow();
            stipendWindow.show();
        });

        settingsBtn.setOnAction(event -> {
            SettingsWindow settingsWindow = new SettingsWindow();
            settingsWindow.show();
        });
    }

    /**
     * Запускает hibernate подключаясь к базе данных
     */
    private void startHibernate() {
        HibernateUtil.buildSessionFactory();
    }

    /**
     * Останавливает работу hibernate
     */
    private void shutdownHibernate() {
        HibernateUtil.shutdown();
    }

    /**
     * Завершает работы приложения
     */
    public void shutdownApp() {
        shutdownHibernate();
        primaryStage.close();
    }

    /**
     * Переключает отображения меню-режимов
     *
     * @param editMode - режим редактирования
     * @param mainMenu - режим гланого меню
     */
    private void switchMode(Boolean editMode, Boolean mainMenu) {
        this.editMode.setVisible(editMode);
        this.mainMenu.setVisible(mainMenu);
    }

    /**
     * Переключает видимость таблиц
     *
     * @param tableViewStudents - таблица с данными студентов
     * @param tableViewPerformance - таблица с данными об успевамости
     */
    private void switchVisibleTables(Boolean tableViewStudents, Boolean tableViewPerformance) {
        this.tableViewStudents.setVisible(tableViewStudents);
        this.tableViewPerformance.setVisible(tableViewPerformance);
    }

    /**
     * Добавляет данные
     */
    private void addData() {
        switch (currentTable) {
            case STUDENTS:
                AddingStudentWindow addingStudentWindow = new AddingStudentWindow(tableViewStudents);
                addingStudentWindow.show();
                break;

            case PERFORMANCES:
                AddingPerformanceWindow addingPerformanceWindow = new AddingPerformanceWindow(tableViewPerformance);
                addingPerformanceWindow.show();
                break;
        }
    }

    /**
     * Редактирует данные
     */
    private void editData() {
        SelectDataWindow selectDataWindow;

        switch (currentTable) {
            case STUDENTS:
                selectDataWindow = new SelectDataWindow(tableViewStudents, currentTable);
                selectDataWindow.show();
                break;

            case PERFORMANCES:
                selectDataWindow = new SelectDataWindow(tableViewPerformance, currentTable);
                selectDataWindow.show();
                break;
        }
    }

    /**
     * Удаляет данные
     */
    private void deleteData() {
        DeletingDataWindow deletingDataWindow;

        switch (currentTable) {
            case STUDENTS:
                deletingDataWindow = new DeletingDataWindow(tableViewStudents, currentTable);
                deletingDataWindow.show();
                break;

            case PERFORMANCES:
                deletingDataWindow = new DeletingDataWindow(tableViewPerformance, currentTable);
                deletingDataWindow.show();
                break;
        }
    }

    /**
     * Устанавливает анимацию кнопке настройки
     */
    private void setSettingsBtnAnimation() {
        RotateTransition rotateIn = new RotateTransition(Duration.millis(600), settingsIcon);
        rotateIn.setByAngle(180);
        settingsBtn.setOnMouseEntered(event -> {
            rotateIn.playFromStart();
        });

        RotateTransition rotateOut = new RotateTransition(Duration.millis(600), settingsIcon);
        rotateOut.setByAngle(-180);
        settingsBtn.setOnMouseExited(event -> {
            rotateOut.playFromStart();
        });
    }
}
