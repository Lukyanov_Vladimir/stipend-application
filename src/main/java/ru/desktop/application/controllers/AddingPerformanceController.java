package ru.desktop.application.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.entities.Student;
import ru.desktop.application.entities.Subject;
import ru.desktop.application.services.StudentService;
import ru.desktop.application.services.SubjectService;
import ru.desktop.application.tables.PerformanceTable;
import ru.desktop.application.tables.SubjectsTable;
import ru.desktop.application.utils.HibernateUtil;

/**
 * Класс-контроллер окна добавления успеваемости
 *
 * @author Лукьянов В. А.
 */
public class AddingPerformanceController {

    @FXML
    private TextField studentNumber;

    @FXML
    private TextField subjectNumber;

    @FXML
    private TextField mark;

    @FXML
    private Button saveBtn;

    @FXML
    private TableView<Subject> tableViewSubjects;

    private TableView<Performance> tableViewPerformances;

    public void setTableViewPerformances(TableView<Performance> tableViewPerformances) {
        this.tableViewPerformances = tableViewPerformances;
    }

    @FXML
    public void initialize() {
        fillTableViewSubjects();

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.getSession();

            StudentService studentService = new StudentService(session);

            Student student = studentService.findById(parseInt(studentNumber.getText()));
            student.addPerformance(createObjectPerformance(session));

            studentService.update(student);
            session.close();

            showTablePerformances();
            fieldsClearText();
        });
    }

    /**
     * Отображает таблицу с учебными предметами
     */
    private void fillTableViewSubjects() {
        SubjectsTable subjectsTable = new SubjectsTable(tableViewSubjects);
        subjectsTable.show();
    }

    /**
     * Создаёт объект успеваемость
     *
     * @param session - сессия
     * @return - объект успевамости
     */
    private Performance createObjectPerformance(Session session) {
        Performance performance = new Performance();
        performance.setMark(parseInt(mark.getText()));
        SubjectService subjectService = new SubjectService(session);
        Subject subject = subjectService.findById(parseInt(subjectNumber.getText()));
        subject.addPerformance(performance);

        return performance;
    }

    /**
     * Преобразует строку к типу int
     *
     * @param str - строка
     * @return - int
     */
    private Integer parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Отображает таблицу с успеваемостями
     */
    private void showTablePerformances() {
        PerformanceTable performanceTable = new PerformanceTable(tableViewPerformances);
        performanceTable.show();
    }

    /**
     * Очищает поля ввода
     */
    private void fieldsClearText() {
        clearField(mark);
        clearField(subjectNumber);
        clearField(studentNumber);
    }

    /**
     * Очищает поле ввода
     *
     * @param textField - поле ввода данных
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
