package ru.desktop.application.controllers;

import com.google.gson.Gson;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import ru.desktop.application.StipendCfg;
import ru.desktop.application.file.File;

/**
 * Класс-контроллер окна настроек
 *
 * @author Лукьянов В. А.
 */
public class SettingsController {

    @FXML
    private Button saveBtn;

    @FXML
    private TextField minStipend;

    @FXML
    public void initialize() {
        saveBtn.setOnAction(event -> {
            Gson gson = new Gson();

            StipendCfg stipendCfg = new StipendCfg();
            stipendCfg.setMinStipend(parseLong(minStipend.getText()));

            String jsonSettings = gson.toJson(stipendCfg);

            File file = new File("src\\main\\resources\\json\\settings.json");
            file.fileWrite(jsonSettings);

            fieldsClearText();
        });
    }

    /**
     * Преобразует строку к типу long
     *
     * @param str - строка
     * @return - long
     */
    private long parseLong(String str) {
        return Long.parseLong(str);
    }


    /**
     * Очищает поля ввода
     */
    private void fieldsClearText() {
        clearField(minStipend);
    }

    /**
     * Очищает поле ввода
     *
     * @param textField - поле ввода данных
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
