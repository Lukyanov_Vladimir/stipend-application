package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ru.desktop.application.controllers.AddingPerformanceController;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна добавления успеваемости
 *
 * @author Лукьянов В. А.
 */
public class AddingPerformanceWindow implements Window {

    private final Stage stage;
    private final AddingPerformanceController addingPerformanceController;
    private final TableView<Performance> tableViewPerformances;

    private Scene scene;

    public AddingPerformanceWindow(TableView<Performance> tableViewPerformances) {
        this.tableViewPerformances = tableViewPerformances;

        stage = new Stage();

        addingPerformanceController = new AddingPerformanceController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Добавление данных об успеваемости студента");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/addPerformance.fxml"));
        setControllerSettings();
        fxmlLoader.setController(addingPerformanceController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        addingPerformanceController.setTableViewPerformances(tableViewPerformances);
    }
}
