package ru.desktop.application.windows;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.desktop.application.controllers.MainController;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения главного окна приложения
 *
 * @author Лукьянов В. А.
 */
public class MainWindow implements Window {

    private final Stage primaryStage;
    private final MainController mainController;
    private final EventHandler<WindowEvent> windowCloseHandler = new EventHandler<WindowEvent>() {
        public void handle(WindowEvent event) {
            mainController.shutdownApp();
        }
    };

    private Scene scene;

    public MainWindow(Stage primaryStage) {
        this.primaryStage = primaryStage;

        mainController = new MainController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        primaryStage.centerOnScreen();
        primaryStage.setTitle("Назначение стипендии учащимся");
        primaryStage.setOnCloseRequest(windowCloseHandler);
        primaryStage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        setControllerSettings();
        fxmlLoader.setController(mainController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        mainController.setPrimaryStage(primaryStage);
    }
}
