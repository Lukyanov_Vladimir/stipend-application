package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.desktop.application.controllers.ErrorWindowController;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна ошибки
 *
 * @author Лукьянов В. А.
 */
public class ErrorWindow implements Window {

    private final Stage stage;
    private final ErrorWindowController errorWindowController;
    private final String text;

    private Scene scene;

    public ErrorWindow(String text) {
        this.text = text;

        stage = new Stage();

        errorWindowController = new ErrorWindowController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Ошибка");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/error.fxml"));
        setControllerSettings();
        fxmlLoader.setController(errorWindowController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        errorWindowController.setStage(stage);
        errorWindowController.setErrorText(text);
    }
}
