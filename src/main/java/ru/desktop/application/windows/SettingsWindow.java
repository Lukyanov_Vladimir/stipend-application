package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.desktop.application.controllers.SettingsController;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна настроек
 *
 * @author Лукьянов В. А.
 */
public class SettingsWindow implements Window {

    private final Stage stage;
    private final SettingsController settingsController;

    private Scene scene;

    public SettingsWindow() {
        stage = new Stage();

        settingsController = new SettingsController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Настройки");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/settings.fxml"));
        fxmlLoader.setController(settingsController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }
}
