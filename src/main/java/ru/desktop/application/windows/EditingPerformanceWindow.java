package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ru.desktop.application.controllers.EditingPerformanceController;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна редактирования успевамости
 *
 * @author Лукьянов В. А.
 */
public class EditingPerformanceWindow implements Window {

    private final Stage stage;
    private final EditingPerformanceController editingPerformanceController;
    private final TableView<Performance> tableViewPerformances;
    private final int performanceNumber;

    private Scene scene;

    public EditingPerformanceWindow(TableView<Performance> tableViewPerformances, int performanceNumber) {
        this.tableViewPerformances = tableViewPerformances;
        this.performanceNumber = performanceNumber;

        stage = new Stage();

        editingPerformanceController = new EditingPerformanceController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Изменение данных об успеваемости студента");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/editPerformance.fxml"));
        setControllerSettings();
        fxmlLoader.setController(editingPerformanceController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        editingPerformanceController.setTableViewPerformances(tableViewPerformances);
        editingPerformanceController.setPerformanceNumber(performanceNumber);
    }
}
