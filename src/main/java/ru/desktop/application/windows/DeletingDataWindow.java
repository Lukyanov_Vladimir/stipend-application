package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ru.desktop.application.Tables;
import ru.desktop.application.controllers.DeletingDataController;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна удаления данных
 *
 * @author Лукьянов В. А.
 */
public class DeletingDataWindow implements Window {

    private final Stage stage;
    private final DeletingDataController deletingDataController;
    private final TableView<?> tableView;
    private final Tables currentTable;

    private Scene scene;

    public DeletingDataWindow(TableView<?> tableView, Tables currentTable) {
        this.tableView = tableView;
        this.currentTable = currentTable;

        stage = new Stage();

        deletingDataController = new DeletingDataController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Удаление данных");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/deleteData.fxml"));
        setControllerSettings();
        fxmlLoader.setController(deletingDataController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        deletingDataController.setTableView(tableView);
        deletingDataController.setCurrentTable(currentTable);
    }
}
