package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ru.desktop.application.controllers.EditingStudentController;
import ru.desktop.application.entities.Student;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна редактирования успевамости
 *
 * @author Лукьянов В. А.
 */
public class EditingStudentWindow implements Window {

    private final Stage stage;
    private final EditingStudentController editingStudentController;
    private final TableView<Student> tableViewStudents;
    private final int studentNumber;

    private Scene scene;

    public EditingStudentWindow(TableView<Student> tableViewStudents, int studentNumber) {
        this.tableViewStudents = tableViewStudents;
        this.studentNumber = studentNumber;

        stage = new Stage();

        editingStudentController = new EditingStudentController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Редактирование данных студента");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/editStudent.fxml"));
        setControllerSettings();
        fxmlLoader.setController(editingStudentController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        editingStudentController.setTableViewStudents(tableViewStudents);
        editingStudentController.setStudentNumber(studentNumber);
    }
}
