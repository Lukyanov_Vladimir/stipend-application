package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ru.desktop.application.Tables;
import ru.desktop.application.controllers.SelectDataController;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна выбора данных
 *
 * @author Лукьянов В. А.
 */
public class SelectDataWindow implements Window {

    private final Stage stage;
    private final SelectDataController selectDataController;
    private final TableView<?> tableView;
    private final Tables currentTable;

    private Scene scene;

    public SelectDataWindow(TableView<?> tableView, Tables currentTable) {
        this.tableView = tableView;
        this.currentTable = currentTable;

        stage = new Stage();

        selectDataController = new SelectDataController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Выбор данных");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/selectData.fxml"));
        setControllerSettings();
        fxmlLoader.setController(selectDataController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        selectDataController.setTableView(tableView);
        selectDataController.setCurrentTable(currentTable);
        selectDataController.setStage(stage);
    }
}
