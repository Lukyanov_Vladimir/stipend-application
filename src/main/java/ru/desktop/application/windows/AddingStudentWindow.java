package ru.desktop.application.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ru.desktop.application.controllers.AddingStudentController;
import ru.desktop.application.entities.Student;
import ru.desktop.application.interfaces.Window;

import java.io.IOException;

/**
 * Класс для отображения окна добавления студента
 *
 * @author Лукьянов В. А.
 */
public class AddingStudentWindow implements Window {

    private final Stage stage;
    private final AddingStudentController addingStudentController;
    private final TableView<Student> tableViewStudents;

    private Scene scene;

    public AddingStudentWindow(TableView<Student> tableViewStudents) {
        this.tableViewStudents = tableViewStudents;

        stage = new Stage();

        addingStudentController = new AddingStudentController();
    }

    /**
     * Показывает окно
     */
    public void show() {
        try {
            loadScene();
            setStageSettings();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Устанавливает настройки самого окна
     */
    private void setStageSettings() {
        stage.centerOnScreen();
        stage.setTitle("Добавление студента");
        stage.setScene(scene);
    }

    /**
     * Загружает сцену для окна
     *
     * @throws IOException - ошибка ввода, вывода
     */
    private void loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/addStudent.fxml"));
        setControllerSettings();
        fxmlLoader.setController(addingStudentController);
        Parent root = fxmlLoader.load();
        scene = new Scene(root);
    }

    /**
     * Устанавливает первоначальные настройки для коректной работы контроллера
     */
    private void setControllerSettings() {
        addingStudentController.setTableViewStudents(tableViewStudents);
    }
}
