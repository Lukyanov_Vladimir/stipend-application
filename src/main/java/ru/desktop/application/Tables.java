package ru.desktop.application;

/**
 * Класс-перечисление таблиц
 *
 * @author Лукьянов В. А.
 */
public enum Tables {
    STUDENTS,
    PERFORMANCES,
}
