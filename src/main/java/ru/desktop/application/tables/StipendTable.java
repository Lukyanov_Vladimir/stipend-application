package ru.desktop.application.tables;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.desktop.application.entities.Student;
import ru.desktop.application.interfaces.Table;

import java.util.List;

/**
 * Класс для отображения данных рассчитанной академической стипендии в таблицу
 *
 * @author Лукьянов В. А.
 */
public class StipendTable implements Table {

    private final TableView<Student> tableView;
    private final List<Student> students;

    public StipendTable(TableView<Student> tableView, List<Student> students) {
        this.tableView = tableView;
        this.students = students;
    }

    /**
     * отображает данные в таблицу
     */
    @Override
    public void show() {
        TableColumn<Student, Integer> numberStudent = new TableColumn<>("№");
        numberStudent.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberStudent.setStyle("-fx-alignment: center");

        TableColumn<Student, String> fullName = new TableColumn<>("ФИО студента");
        fullName.setCellValueFactory(new PropertyValueFactory<>("fullName"));

        TableColumn<Student, String> stipends = new TableColumn<>("Стипендия");
        stipends.setCellValueFactory(new PropertyValueFactory<>("stipend"));

        showData(numberStudent, fullName, stipends);
    }

    /**
     * Отображает данные
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void showData(TableColumn<Student, ?>... columns) {
        clearTable(tableView);
        setItems(getObservableList());
        addColumns(columns);
    }

    /**
     * Устанавливает список значений в таблицу
     *
     * @param observableList - обсервативный список
     */
    private void setItems(ObservableList<Student> observableList) {
        tableView.setItems(observableList);
    }

    /**
     * Очищает таблицу
     *
     * @param table - таблица
     */
    private void clearTable(TableView<Student> table) {
        table.getColumns().clear();
    }

    /**
     * Метод возвращает обсервативный список с успеваемостями
     *
     * @return - обсервативный список
     */
    private ObservableList<Student> getObservableList() {
        return FXCollections.observableList(students);
    }

    /**
     * Добавляет колонки в таблицу
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void addColumns(TableColumn<Student, ?>... columns) {
        tableView.getColumns().addAll(columns);
    }
}
