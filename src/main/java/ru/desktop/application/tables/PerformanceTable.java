package ru.desktop.application.tables;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.interfaces.Table;
import ru.desktop.application.services.PerformanceService;
import ru.desktop.application.utils.HibernateUtil;

import java.util.List;

/**
 * Класс для отображения данных об успеваемости в таблицу
 *
 * @author Лукьянов В. А.
 */
public class PerformanceTable implements Table {

    private final TableView<Performance> tableView;

    public PerformanceTable(TableView<Performance> tableView) {
        this.tableView = tableView;
    }

    /**
     * отображает данные в таблицу
     */
    @Override
    public void show() {
        TableColumn<Performance, Integer> numberPerformance = new TableColumn<>("№");
        numberPerformance.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberPerformance.setStyle("-fx-alignment: center");

        TableColumn<Performance, String> fullNameStudent = new TableColumn<>("ФИО студента");
        fullNameStudent.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getStudent().getFullName()
        ));

        TableColumn<Performance, Integer> mark = new TableColumn<>("Оценка");
        mark.setCellValueFactory(new PropertyValueFactory<>("mark"));
        mark.setStyle("-fx-alignment: center");

        TableColumn<Performance, String> subject = new TableColumn<>("Дисциплина");
        subject.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getSubject().getSubjectName()
        ));

        TableColumn<Performance, String> teacher = new TableColumn<>("ФИО преподавателя");
        teacher.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getSubject().getFullNameTeacher()
        ));

        showData(numberPerformance, fullNameStudent, mark, subject, teacher);
    }

    /**
     * Отображает данные
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void showData(TableColumn<Performance, ?>... columns) {
        clearTable(tableView);
        Session session = HibernateUtil.getSession();
        PerformanceService performanceService = new PerformanceService(session);
        setItems(getObservableList(performanceService.findAll()));
        session.close();
        addColumns(columns);
    }

    /**
     * Устанавливает список значений в таблицу
     *
     * @param observableList - обсервативный список
     */
    private void setItems(ObservableList<Performance> observableList) {
        tableView.setItems(observableList);
    }

    /**
     * Очищает таблицу
     *
     * @param table - таблица
     */
    private void clearTable(TableView<Performance> table) {
        table.getColumns().clear();
    }

    /**
     * Метод возвращает обсервативный список с успеваемостями
     *
     * @param performances - список с успеваемостями
     * @return - обсервативный список
     */
    private ObservableList<Performance> getObservableList(List<Performance> performances) {
        return FXCollections.observableList(performances);
    }

    /**
     * Добавляет колонки в таблицу
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void addColumns(TableColumn<Performance, ?>... columns) {
        tableView.getColumns().addAll(columns);
    }
}
