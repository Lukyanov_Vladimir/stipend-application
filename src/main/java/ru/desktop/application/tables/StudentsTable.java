package ru.desktop.application.tables;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;
import ru.desktop.application.entities.Student;
import ru.desktop.application.interfaces.Table;
import ru.desktop.application.services.StudentService;
import ru.desktop.application.utils.HibernateUtil;

import java.util.List;

/**
 * Класс для отображения данных о студентах в таблицу
 *
 * @author Лукьянов В. А.
 */
public class StudentsTable implements Table {

    private final TableView<Student> tableView;

    public StudentsTable(TableView<Student> tableView) {
        this.tableView = tableView;
    }

    /**
     * отображает данные в таблицу
     */
    @Override
    public void show() {
        TableColumn<Student, Integer> numberStudent = new TableColumn<>("№");
        numberStudent.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberStudent.setStyle("-fx-alignment: center");

        TableColumn<Student, String> fullName = new TableColumn<>("ФИО студента");
        fullName.setCellValueFactory(new PropertyValueFactory<>("fullName"));

        TableColumn<Student, String> address = new TableColumn<>("Адрес проживания");
        address.setCellValueFactory(new PropertyValueFactory<>("address"));

        TableColumn<Student, Long> phoneNumber = new TableColumn<>("Номер телефона");
        phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        phoneNumber.setStyle("-fx-alignment: center");

        showData(numberStudent, fullName, address, phoneNumber);
    }

    /**
     * Отображает данные
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void showData(TableColumn<Student, ?>... columns) {
        clearTable(tableView);
        Session session = HibernateUtil.getSession();
        StudentService studentService = new StudentService(session);
        setItems(getObservableList(studentService.findAll()));
        session.close();
        addColumns(columns);
    }

    /**
     * Устанавливает список значений в таблицу
     *
     * @param observableList - обсервативный список
     */
    private void setItems(ObservableList<Student> observableList) {
        tableView.setItems(observableList);
    }

    /**
     * Очищает таблицу
     *
     * @param table - таблица
     */
    private void clearTable(TableView<Student> table) {
        table.getColumns().clear();
    }

    /**
     * Метод возвращает обсервативный список с успеваемостями
     *
     * @param students - список со студентами
     * @return - обсервативный список
     */
    private ObservableList<Student> getObservableList(List<Student> students) {
        return FXCollections.observableList(students);
    }

    /**
     * Добавляет колонки в таблицу
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void addColumns(TableColumn<Student, ?>... columns) {
        tableView.getColumns().addAll(columns);
    }
}
