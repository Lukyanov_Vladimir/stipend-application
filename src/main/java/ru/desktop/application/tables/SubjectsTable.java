package ru.desktop.application.tables;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;
import ru.desktop.application.entities.Subject;
import ru.desktop.application.interfaces.Table;
import ru.desktop.application.services.SubjectService;
import ru.desktop.application.utils.HibernateUtil;

import java.util.List;

/**
 * Класс для отображения данных об учебных предметах в таблицу
 *
 * @author Лукьянов В. А.
 */
public class SubjectsTable implements Table {

    private final TableView<Subject> tableView;

    public SubjectsTable(TableView<Subject> tableView) {
        this.tableView = tableView;
    }

    /**
     * отображает данные в таблицу
     */
    @Override
    public void show() {
        TableColumn<Subject, Integer> numberSubject = new TableColumn<>("№");
        numberSubject.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberSubject.setStyle("-fx-alignment: center");

        TableColumn<Subject, String> nameSubject = new TableColumn<>("Название дисциплины");
        nameSubject.setCellValueFactory(new PropertyValueFactory<>("subjectName"));

        TableColumn<Subject, String> fullNameTeacher = new TableColumn<>("ФИО учителя");
        fullNameTeacher.setCellValueFactory(new PropertyValueFactory<>("fullNameTeacher"));

        showData(numberSubject, nameSubject, fullNameTeacher);
    }

    /**
     * Отображает данные
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void showData(TableColumn<Subject, ?>... columns) {
        clearTable(tableView);
        Session session = HibernateUtil.getSession();
        SubjectService subjectService = new SubjectService(session);
        setItems(getObservableList(subjectService.findAll()));
        session.close();
        addColumns(columns);
    }

    /**
     * Устанавливает список значений в таблицу
     *
     * @param observableList - обсервативный список
     */
    private void setItems(ObservableList<Subject> observableList) {
        tableView.setItems(observableList);
    }

    /**
     * Очищает таблицу
     *
     * @param table - таблица
     */
    private void clearTable(TableView<Subject> table) {
        table.getColumns().clear();
    }

    /**
     * Метод возвращает обсервативный список с успеваемостями
     *
     * @param subjects - список с учебными предметами
     * @return - обсервативный список
     */
    private ObservableList<Subject> getObservableList(List<Subject> subjects) {
        return FXCollections.observableList(subjects);
    }

    /**
     * Добавляет колонки в таблицу
     *
     * @param columns - массив колонок
     */
    @SafeVarargs
    private final void addColumns(TableColumn<Subject, ?>... columns) {
        tableView.getColumns().addAll(columns);
    }
}
