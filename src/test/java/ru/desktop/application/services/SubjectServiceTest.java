package ru.desktop.application.services;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;
import ru.desktop.application.entities.Subject;
import ru.desktop.application.utils.HibernateUtil;

public class SubjectServiceTest {

    private Subject actualSubject;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.getSession();
        actualSubject = createSubject();
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        SubjectService subjectService = new SubjectService(session);

        subjectService.save(actualSubject);

        Subject expectedSubject = subjectService.findById(actualSubject.getId());

        return actualSubject.equals(expectedSubject);
    }


    private boolean update(Session session) {

        SubjectService subjectService = new SubjectService(session);

        actualSubject.setSubjectName("Непредмет");
        actualSubject.setFullNameTeacher("Неучитель");

        subjectService.update(actualSubject);

        Subject expectedSubject = subjectService.findById(actualSubject.getId());

        return actualSubject.equals(expectedSubject);
    }


    private boolean delete(Session session) {

        SubjectService subjectService = new SubjectService(session);

        subjectService.delete(actualSubject);

        Subject subject = subjectService.findById(actualSubject.getId());

        return subject == null;
    }

    private Subject createSubject() {

        Subject subject = new Subject();
        subject.setSubjectName("Предмет");
        subject.setFullNameTeacher("Учитель");

        return subject;
    }
}