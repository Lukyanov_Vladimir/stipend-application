package ru.desktop.application.services;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;
import ru.desktop.application.entities.Student;
import ru.desktop.application.utils.HibernateUtil;

public class StudentServiceTest {

    private Student actualStudent;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.getSession();
        actualStudent = createStudent();
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        StudentService studentService = new StudentService(session);

        studentService.save(actualStudent);

        Student expectedStudent = studentService.findById(actualStudent.getId());

        return actualStudent.equals(expectedStudent);
    }


    private boolean update(Session session) {

        StudentService studentService = new StudentService(session);

        actualStudent.setFullName("ФиО");
        actualStudent.setAddress("Не адрес");
        actualStudent.setPhoneNumber(231233123L);

        studentService.update(actualStudent);

        Student expectedStudent = studentService.findById(actualStudent.getId());

        return actualStudent.equals(expectedStudent);
    }


    private boolean delete(Session session) {

        StudentService studentService = new StudentService(session);

        studentService.delete(actualStudent);

        Student student = studentService.findById(actualStudent.getId());

        return student == null;
    }

    private Student createStudent() {

        Student student = new Student();
        student.setFullName("ФИО");
        student.setAddress("Адрес");
        student.setPhoneNumber(231233123L);

        return student;
    }
}