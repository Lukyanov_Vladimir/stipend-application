package ru.desktop.application.services;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;
import ru.desktop.application.entities.Performance;
import ru.desktop.application.entities.Student;
import ru.desktop.application.entities.Subject;
import ru.desktop.application.utils.HibernateUtil;;

public class PerformanceServiceTest {

    private Performance actualPerformance;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.getSession();

        Student student = createStudent();
        StudentService studentService = new StudentService(session);
        studentService.save(student);

        actualPerformance = createPerformance(student, session);
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        studentService.delete(student);
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        PerformanceService performanceService = new PerformanceService(session);

        performanceService.save(actualPerformance);

        Performance expectedPerformance = performanceService.findById(actualPerformance.getId());

        return actualPerformance.equals(expectedPerformance);
    }


    private boolean update(Session session) {

        PerformanceService performanceService = new PerformanceService(session);

        actualPerformance.setMark(2);

        performanceService.update(actualPerformance);

        Performance expectedPerformance = performanceService.findById(actualPerformance.getId());

        return actualPerformance.equals(expectedPerformance);
    }


    private boolean delete(Session session) {

        PerformanceService performanceService = new PerformanceService(session);

        performanceService.delete(actualPerformance);

        Performance performance = performanceService.findById(actualPerformance.getId());

        return performance == null;
    }

    private Performance createPerformance(Student student, Session session) {

        Performance performance = new Performance();
        performance.setMark(5);
        performance.setStudent(student);
        SubjectService subjectService = new SubjectService(session);

        Subject subject = subjectService.findById(1);
        subject.addPerformance(performance);

        return performance;
    }

    private Student createStudent() {

        Student student = new Student();
        student.setFullName("ФИО");
        student.setAddress("Адрес");
        student.setPhoneNumber(231233123L);

        return student;
    }
}